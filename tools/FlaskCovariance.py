# Calculates the covariance of Flask map simulations.
#
# Author: Felipe SC
# Email: felipesc.physis@gmail.com, felipesc.physis@protonmail.com
# Project: BINGO

import time
import glob
import os

import numpy as np
import multiprocessing as mp
from natsort import natsorted


cores = 15

pathFolder = "PATH/dados/output/FLASKmaps/OverdensityBined9"
bins = natsorted(glob.glob(pathFolder + '/z*'))

bindatapath = []
for k in range(len(bins)):
    bindatapath.append(glob.glob(bins[k]+"/Flask*.dat"))

bined = 'Bined' in pathFolder.split('/')[-1] # Parametro para definir formato dos dados. Cl's binados não possuem cabeçalho no inicio do documento.


def clMedia(binNum = int):
    """
    Calculates the average of the Power spectrum of the simulated Flask maps.
       
    output:  ..znzn/clmedio.dat
    """
    segundosi = time.time()

    # Opening files, skipping irrelevant lines with skiprows=skip.
    numberOfSimulations = len(bindatapath[binNum])
    for k in range(numberOfSimulations):
        # Defining 'skiprows' depending on the data set.
        if bined==True:
            skip = 0
        else:
            z = bindatapath[binNum][k].split("/")[-2].split('z')
            if z[1]==z[2]:
                skip = 2
            else:
                skip = 4
        
        if k==0:
            media = np.loadtxt(bindatapath[binNum][k],skiprows=skip)[:,1]/numberOfSimulations
        else:
            media = media + np.loadtxt(bindatapath[binNum][k],skiprows=skip)[:,1]/numberOfSimulations
        
        # Capturing values of l in the last round:
        if k==numberOfSimulations-1:
            l = np.loadtxt(bindatapath[binNum][k],skiprows=skip)[:,0]
        
    # Saving data
    mediaSalvar = np.concatenate([l.reshape((len(media),1)),media.reshape((len(media),1))],axis=1)

    tituloClMedio = "l and the average Cl's of the simulations."
    np.savetxt(str(bins[binNum]+'/clmedio.dat'), mediaSalvar, fmt=['%i','%.6e'], header=tituloClMedio)
    
    segundosf = time.time()
    print(str(int(100*binNum/len(bins))),"%",'finalizado')
    print('tempo[s]: ',format(segundosf-segundosi))


def covariancia(binNum = int): # binNum: bin number
    """
    Calculates the covariances of each redshift bin from simulated Flask maps.
       
    Output:
    Cov(bin_ij, bin_kl) for all i,j,k,l in ..covMatrix/...dat 
        
    (e.g.: ..covMatrix/z0z0-z0z0.dat, i,j,k,l = 0)
    """
    segundosi = time.time()
    
    # Creating directory to store data
    os.system(str('mkdir -p ' + pathFolder+'/covMatrix')) # mkdir -p: No erro if exists
    
    numberOfSimulations = len(bindatapath[binNum])
    
    path = bins[binNum]+'/clmedio.dat'
    media = np.loadtxt(path)[:,1]
    
    lenMedia = len(media)
    lenBins = len(bins)
    for bCross in range(binNum,lenBins):
#     for bCross in range(binNum,binNum+1):
        path = bins[bCross]+'/clmedio.dat'
        mediaCross = np.loadtxt(path)[:,1]
        
        covariancia = np.zeros((lenMedia,lenMedia)) # Matriz quadrada
        for k in range(numberOfSimulations): # k-simulação 
            # Open Cl's
            if bined==True:
                skip = 0
            else:
                z = bindatapath[binNum][k].split("/")[-2].split('z')
                if z[1]==z[2]:
                    skip = 2
                else:
                    skip = 4
            cl = np.loadtxt(bindatapath[binNum][k],skiprows=skip)[:,1] # bindatapath[bin][simulation number][:,0=l,1=Cl]
            
            # Open clCross
            if bined==True:
                skip = 0
            else:
                z = bindatapath[bCross][k].split("/")[-2].split('z')
                if z[1]==z[2]:
                    skip = 2
                else:
                    skip = 4
            clCross = np.loadtxt(bindatapath[bCross][k],skiprows=skip)[:,1] # bindatapath[bin][simulation number][:,0=l,1=Cl]
            
            # Covariance 
            for l in range(lenMedia): # Linha da matriz
                for c in range(lenMedia): # Coluna da matriz
                    covariancia[l,c] = covariancia[l,c] + (cl[l] - media[l])*(clCross[c] - mediaCross[c]) / (numberOfSimulations-1)

        # Saving data
        pathSave = pathFolder+'/covMatrix/'+bins[binNum].split("/")[-1]+'-'+bins[bCross].split("/")[-1]+'.dat'
        np.savetxt(pathSave, covariancia, fmt='%.5e')
    
    segundosf = time.time()
    print(str(int(100*binNum/len(bins))),"%",'finalizado')
    print('tempo[s]: ',format(segundosf-segundosi))


## By combining all the simulated covariance matrices into a single covariance matrix.
def covMatrix(path = pathFolder, output = ''):
    """
    Takes the covariance of specific bins and concatenates in the covariance matrix.
    pathInput = Flask/simulations/folder/path (string).
    output = Output/path
    """
    segundosi = time.time()

    bins = 14

    count = 0
    covBinPaths = []
    for k in range(bins):
        for i in range(k,bins):
            tempPaths = glob.glob(path+"/covMatrix/z"+str(k)+'z'+str(i)+"-*.dat")
            covBinPaths.append(tempPaths)
            count = count + 1
    bins = count

    # Concatenating the elements of the symmetric triangular matrix.
    for i in range(bins): # Line
        for j in range(bins): # Columns
            if i<j:
                covBin = np.loadtxt(covBinPaths[i][j-i]) # Aij
            else:
                covBin = np.loadtxt(covBinPaths[j][i-j]) # Aji

            # Concatenating
            if j==0:
                line = covBin
            else:
                line = np.append(line,covBin,axis=1) 
        if i==0:
            matrizOut = line
        else:
            matrizOut = np.append(matrizOut,line,axis=0)


        print(str(int(100*(i+1)/bins)),"% Finalizado")

        segundosf = time.time()
        print('tempo[s]: ', format(segundosf-segundosi))

    np.savetxt(path+"/CovMatrix.dat", matrizOut, fmt='%.5e')


def run():
    # Mean
    pool = mp.Pool(cores)
    for b in range(len(bins)):
        pool.apply_async(clMedia,args=(b,))
    pool.close()
    pool.join()
    
    # Covariance
    pool = mp.Pool(cores)
    for b in range(len(bins)):
        pool.apply_async(covariancia,args=(b,))
    pool.close()
    pool.join()
    
    # Covariance Matrix
    covMatrix()


if __name__ == "__main__":
    run()

