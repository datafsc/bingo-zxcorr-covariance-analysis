# Easy Flask Simulation Calculation:
# Error calculation in the cross correlation between Map-Map and 
# Map-Systematics using FLASK simulations.
#
# Author: Felipe SC
# Email: felipesc.physis@gmail.com, felipesc.physis@protonmail.com
# Version: 6.0

import glob
import os

import time

# Parallelization
import multiprocessing as mp

# ==========================================
# FUNCTIONS
# ==========================================
from EsyFSC.delheader import delheader
from EsyFSC.dens2overdens import dens2overdens
from EsyFSC.flasksimulationcl import flasksimulationcl
import EsyFSC.pseudopower
from EsyFSC.stdvcls import stdvcls
import EsyFSC.bining
import EsyFSC.covariance


# ==========================================
# CONFIGURATIONS
# ==========================================
from configparser import ConfigParser 

# Loading config file 'esy.ini'.
ini = './esy.ini'
cfg = ConfigParser(inline_comment_prefixes="#")
cfg.read(ini)
config = dict(cfg.items('PARAMETERS'))


if __name__ == "__main__":
    inicio = time.process_time()
    start = str(time.ctime())
    segundosi = time.time()

    # ===========================================  
    # SKY-MAPS CORRELATION
    # ===========================================  
    if int(config['calccros']) == 1:
        # ----------
        # SKY-MAPS
        # ----------
        # Map2Alm
        EsyFSC.pseudopower.map2alm(pathMaps=config['mapsdir'], almDir=config['mapsalmdir'])
        
        # Alm2PS: Sl    (PS-Power Spectrum, Cl=Sl+Shotnoise)
        EsyFSC.pseudopower.alm2ps(pathMaps=config['mapsdir'], almDir=config['mapsalmdir'], rm_shotnoise = True)
        
        # Alm2PS: Cl    (PS-Power Spectrum, Cl=Sl+Shotnoise)
        EsyFSC.pseudopower.alm2ps(pathMaps=config['mapsdir'], almDir=config['mapsalmdir'], rm_shotnoise = False)
        

    if int(config['sys_cl'])==1:
        # ---------------------
        # SYSTEMATICS-MAPS Alm
        # ---------------------
        # Map2Alm
        EsyFSC.pseudopower.map2alm(pathMaps=config['systematicsmaps'], almDir=config['sys_almdir'])
        
        # Alm2Cl
        EsyFSC.pseudopower.alm2clSystematics()

    # Delet fist line n-times; n = 'del_header' option in esy.ini.
    # This is done so that FLASK can read the sls.dat correctly.
    if int(config['del_header']) > 0:
        # For the Cls
        path = str(config['clmapsdir']+'/cls'+ '/*.dat')
        delheader(path, NUM=int(config['del_header']))
        
        # For the Sls
        path = str(config['clmapsdir']+'/sls'+ '/*.dat')
        delheader(path, NUM=int(config['del_header']))
        

    # ===========================================  
    # ERRO CALCULATION 
    # ===========================================
    # N-Sky maps simulation with FLASK.
    if int(config['nflaskmaps']) > 0:
        pool = mp.Pool(int(config['ncores'])) # Parallelization. Open a processing pool with 'ncores'. 'ncores' = integer
        for k in range(int(config['nflaskmaps'])):        
            pool.apply_async(flasksimulationcl, args=(k+int(config['start_in']),) )
        pool.close()
        pool.join()

    # ----------
    # FLASK-MAPS
    # ----------
    # Overdensity Flask-MAPS: Standard deviation Calculation
    if int(config['over_flaskmap_stdv_calc'])==1:
        binpath = glob.glob(str(config['over_flaskmaps_autocl']+'/z*'))
        stdvcls(binpath)
    
    # Density_to_Overdensity Flask-MAPS: Standard deviation Calculation
    if int(config['dens_to_over_flaskmap_stdv_calc'])==1:
        binpath = glob.glob(str(config['dens_to_over_flaskmaps_autocl']+'/z*'))
        stdvcls(binpath)

    # ----------
    # SYS-MAPS
    # ----------
    # Overdensity SYS-MAPS: Standard deviation Calculation
    if int(config['over_sysmap_stdv_calc'])==1: 
        binpath = glob.glob(str(config['over_flaskmaps_cross_sys']+'/*/*'))
        stdvcls(binpath, systematics=True)
    
    # Density_to_Overdensity SYS-MAPS: Standard deviation Calculation
    if int(config['dens_to_over_sysmap_stdv_calc'])==1: 
        binpath =  glob.glob(str(config['dens_to_over_flaskmaps_cross_sys']+'/*/*'))
        stdvcls(binpath, systematics=True)
    
    
    # ===========================================  
    # COVARIANCE
    # ===========================================  
    
    # --------------------
    # BINING FLASK-MAPS
    # -------------------- 
    if int(config['bining']) > 0:
        EsyFSC.bining.flaskmaps()

    # -----------------------
    # FLASK MAPS COVARIANCE
    # -----------------------
    # Overdensity Covariance 
    if int(config['covariance_over']) == 1:
        path = glob.glob(config['over_flaskmaps_autocl']+"Bined*") # Get all folders with "Bined" in the name
        for k in range(len(path)):
            EsyFSC.covariance.Flask(path[k], cores=int(config['ncores'])).run()
    
    # Density_to_Overdensity Covariance 
    if int(config['covariance_dens_to_over']) == 1:
        path = glob.glob(config['dens_to_over_flaskmaps_autocl']+"Bined*")
        for k in range(len(path)):
            EsyFSC.covariance.Flask(path[k], cores=int(config['ncores'])).run()
    

    fim = time.process_time()
    segundosf = time.time()
    print('start:',start[11:-5])
    print('Finish:',str(time.ctime())[11:-5])
    print('Process Time: ',fim-inicio)
    print('min: ',format(round((segundosf-segundosi)/60,2)))
    #print('start:',start[11:-5])
    #print('Finish:',str(time.ctime())[11:-5])
