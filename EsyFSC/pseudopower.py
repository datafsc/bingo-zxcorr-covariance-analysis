# Calculates Power Spectrum (PS) from real maps usign PseudoPower code.
#
# Author: Felipe SC
# Email: felipesc.physis@gmail.com, felipesc.physis@protonmail.com
# Project: BINGO

import glob
import os
from natsort import natsorted

# ==========================================
# CONFIGURATIONS
# ==========================================
from configparser import ConfigParser 

# Loading config file 'esy.ini'.
ini = './esy.ini'
cfg = ConfigParser(inline_comment_prefixes="#")
cfg.read(ini)
config = dict(cfg.items('PARAMETERS'))


def map2alm(pathMaps, almDir):
    """
    Calls the Map2Alm function from PseudoPower code.

    Input:
    pathMaps - PATH/maps.fits
    almDir - PATH/output/Alm.fits 
    esy.ini - PATH/mask.fits and PseudoPower directory.
    
    Output: Alm.fits files
    """
    # Maps: Map2Alm
    mapsbins = glob.glob(str(pathMaps + '/*.fits'))
    os.system(str('mkdir -p ' + almDir))
    k = 0
    for k in range(len(mapsbins)):
        comand = str(config['pseudopowerdir'] 
                     + '/Map2Alm -I ' + mapsbins[k] 
                     + ' -O ' + almDir + mapsbins[k][len(pathMaps):-5] + '_Alm.fits' 
                     + ' -m ' + config['footprintmask'])
        os.system(comand)
    
    
def alm2ps(pathMaps, almDir, rm_shotnoise = True):
    """
    Calls the Alm2Cl function from PseudoPower code.
    Calculates Power Spectrum from real maps usign PseudoPower code.

    Input:
    pathMaps - PATH/maps.fits
    almDir - PATH/output/Alm.fits 
    rm_shotnoise - Remove_shotnoise.
    esy.ini - configs.
    
    Output: PowerSpectrum.dat files. Cl = Sl + Shotnoise.
    """
    if rm_shotnoise:
        flag_s = ' -s '
        folder = 'sls'
        prefix = 'Sl-'
        
    else:
        flag_s = ' '
        folder = 'cls'
        prefix = 'Cl-'
    
    # Maps: Alm2Cl - mapbin[j]-cross-mapbin[i]

    mapsbins = natsorted(glob.glob(str(pathMaps + '/*.fits')))
    mapsAlms = natsorted(glob.glob(str(almDir + '/*.fits')))
    os.system(str('mkdir -p ' + config['clmapsdir'] + '/'+folder))
    k,j = 0,0
    for k in range(len(mapsbins)):
        for j in range(k,len(mapsbins)): # bin1-cross-bin2 = bin2-cross-bin1

            if k==j: # Autocorrelation flags: -o -s (Remove shotnoise)
                # FLASK notation f1z1 = Field 1 Bin-Redshift 1. (Cannot start at zero)
                comand = str(config['pseudopowerdir']
                             +'/Alm2Cl -I ' + mapsAlms[k] 
                             + ' -O ' + config['clmapsdir']+'/'+folder+'/'+prefix+'f1'+'z'+str(k+1)+'f1'+'z'+str(j+1)+'.dat' 
                             + ' -P -m ' +  config['footprintmask'] 
                             + ' -o -R ' + config['footprint_ilmjlm']
                             + ' -T ' + mapsbins[k] 
                             + flag_s + '-N ' + str(config['nside']) 
                             + ' -L ' + str(config['lmax']) )

            else: # Autocorrelation flags: -o -c 
                comand = str(config['pseudopowerdir'] 
                             +'/Alm2Cl -I ' + mapsAlms[k] 
                             + ' -O ' + config['clmapsdir']+'/'+folder+'/'+prefix+'f1'+'z'+str(k+1)+'f1'+'z'+str(j+1)+'.dat' 
                             + ' -P -m ' + config['footprintmask'] 
                             + ' -o -R ' + config['footprint_ilmjlm'] 
                             + ' -T ' + mapsbins[k]
                             + ' -N ' + str(config['nside']) 
                             + ' -L ' + str(config['lmax']) 
                             + ' -c ' + mapsAlms[j] + ' ' + mapsbins[j] )
            os.system(comand)


def alm2clSystematics():
    # === Systemátics Maps Cl's ===
    
    skymaps = natsorted(glob.glob(str(config['mapsdir'] +"/*")))
    mapsAlm = natsorted(glob.glob(config['mapsalmdir']+"/*"))
    
    # Overdensity FlaskMaps-Systematics: Alm2Cl
    if 1==1:
        folderSys = natsorted(glob.glob(str(config['systematicsmaps'] + '/*.fits')))
        folderSysAlms = natsorted(glob.glob(str(config['sys_almdir'] + '/*.fits')))

        for l in range(len(skymaps)):
            for m in range(len(folderSys)):
                os.system(str('mkdir -p ' + config['sys_dir'] + '/bin'+str(l) ))

                comand = str(config['pseudopowerdir']+'/Alm2Cl -I ' + mapsAlm[l] 
                            + ' -O ' + config['sys_dir']+'/bin'+ str(l)
                            + '/bin'+ str(l)+ '-c-' 
                            + folderSys[m][len(config['systematicsmaps'] +'/'+ config['sys_prefix'] ):-len(config['sys_sufix'])] +'.dat' 
                            + ' -m ' +  config['footprintmask'] 
                            + ' -o -R ' + config['footprint_ilmjlm']
                            + ' -N ' + str(config['nside']) + ' -L ' + str(config['lmax'])
                            + ' -c ' + folderSysAlms[m] + ' ' + folderSys[m])
                os.system(comand)

