# It performs n-simulations of sky maps and calculates the power spectrum.
# Measures correlation with systematic maps.
# Calculates the error.
#
# Author: Felipe SC
# Email: felipesc.physis@gmail.com, felipesc.physis@protonmail.com
# Project: BINGO

from random import uniform

import os 
import glob
from natsort import natsorted

# ==========================================
# CONFIGURATIONS
# ==========================================
from configparser import ConfigParser 

# Loading config file 'esy.ini'.
ini = './esy.ini'
cfg = ConfigParser(inline_comment_prefixes="#")
cfg.read(ini)
config = dict(cfg.items('PARAMETERS'))

# ==========================================
# FUNCTIONS
# ==========================================
from EsyFSC.dens2overdens import dens2overdens


def flasksimulationcl(k):
    '''
    k-Sky maps simulations with Flask to obtain the standard deviation (erro) of Cl's in each l.
    k = Number of the current simulation (integer).
    '''
    cwd = os.getcwd() # Current directory

    # ===================  Flask maps: k-Simulations  ===================
    
    comand = str('mkdir -p ' + config['flaskmapsdir'])
    os.system(comand)
    #FlaskMaps simulation and cl's calculation.

    # Flask maps: k-Simulations
    comand = str('cd' + ' ' + config['flaskdir'] + ' \n ' 
                    + './bin/flask' + ' '+ config['flaskdir'] + config['flaskcfgname']+ ' ' 
                    +  'RNDSEED: ' + str(int(uniform(0,137*70))) + ' '
                    + 'CL_PREFIX: ' + str(config['sl_prefix']) + ' '
                    + 'MAPFITS_PREFIX: ' + config['mapfits_prefix']  + str(k) + '-' + ' '
                    + 'MAPWERFITS_PREFIX: ' + config['mapwerfits_prefix']  + str(k) + '-')
    os.system(comand)
    
    
    Overdensity_FLASK_maps = natsorted(glob.glob(str(config['mapfits_prefix'] + str(k)+"*")))
    Density_FLASK_maps = natsorted(glob.glob(str(config['mapwerfits_prefix'] + str(k)+"*")))
        
    # Density to Overdensity map convert
    for j in range(len(Density_FLASK_maps)):
        dens2overdens(Density_FLASK_maps[j])
        
    Dens_to_Over_FLASK_maps = Density_FLASK_maps

    # ===================  Alm's  ===================
    
    # # Overdensity Flask maps: Map2Alm
    if int(config['over_flask_autocl_calc'])==1 or int(config['over_flaskmaps_cross_sys_calc'])==1:
        comand = str('mkdir -p ' + config['over_flaskmaps_almdir'])
        os.system(comand)

        for j in range(len(Overdensity_FLASK_maps)):
            comand = str(config['pseudopowerdir'] 
                          + '/Map2Alm -I ' + Overdensity_FLASK_maps[j] 
                          + ' -O ' + config['over_flaskmaps_almdir'] + Overdensity_FLASK_maps[j][len(config['flaskmapsdir']):-5] + '_Alm.fits' 
                          + ' -m ' + config['footprintmask'])
            os.system(comand)
            
        Overdensity_Flask_Alm = natsorted(glob.glob(str(config['over_flaskmaps_almdir'] 
                                                 + config['mapfits_prefix'][len(config['flaskmapsdir']):]
                                                 + str(k) + "-" + "*" )))
    
    # # Density_to_Overdensity Flask maps: Map2Alm
    if int(config['dens_to_over_flask_autocl_calc'])==1 or int(config['dens_to_over_flaskmaps_cross_sys_calc'])==1:
        comand = str('mkdir -p ' + config['dens_to_over_flaskmaps_almdir'])
        os.system(comand)

        for j in range(len(Dens_to_Over_FLASK_maps)):
            comand = str(config['pseudopowerdir'] 
                          + '/Map2Alm -I ' + Dens_to_Over_FLASK_maps[j] 
                          + ' -O ' + config['dens_to_over_flaskmaps_almdir'] + Dens_to_Over_FLASK_maps[j][len(config['flaskmapsdir']):-5] + '_Alm.fits' 
                          + ' -m ' + config['footprintmask'])
            os.system(comand)
            
        Dens_to_Over_Flask_Alm = natsorted(glob.glob(str(config['dens_to_over_flaskmaps_almdir'] 
                                                 + config['mapwerfits_prefix'][len(config['flaskmapsdir']):]
                                                 + str(k) + "-" + "*" )))
        

    # ===================  Cl's  ===================
    
    # Overdensity Flask maps: Alm2Cl - FLASKmapbin[j] - Cross - FLASKmapbin[i]
    if int(config['over_flask_autocl_calc'])==1:

        #Checking if paths exist
        for j in range(len(Overdensity_Flask_Alm)):
            for i in range(j,len(Overdensity_Flask_Alm)):
                # Does the path exist? if not:
                if glob.glob(str(config['over_flaskmaps_autocl'] + '/z'+str(j)+'z'+str(i))) == []: 
                    # Folder creation for Overdensity Cl's.
                    for j in range(len(Overdensity_Flask_Alm)):
                        for i in range(j,len(Overdensity_Flask_Alm)):
                            # mkdir -p, --parents. No error if existing, make parent directories as needed.
                            os.system(str('mkdir -p ' + config['over_flaskmaps_autocl'] + '/z'+str(j)+'z'+str(i) )) 
                    break
                            
        # Cl's calc:
        for j in range(len(Overdensity_Flask_Alm)):
            for i in range(j,len(Overdensity_Flask_Alm)):
                # Auto-Cl:
                if i == j: 
                    comand = str(config['pseudopowerdir']+'/Alm2Cl -I ' + Overdensity_Flask_Alm[j]
                                 + ' -O ' + config['over_flaskmaps_autocl'] + '/z'+str(j)+'z'+str(i) + '/FlaskAutoCl-'
                                 + str(k) + '.dat'
                                 + ' -m ' +  config['footprintmask'] 
                                 + ' -o -R ' + config['footprint_ilmjlm']
                                 + ' -N ' + str(config['nside']) 
                                 + ' -L ' + str(config['lmax']) )
                    os.system(comand)
                # Cross-Cl:
                else:
                    comand = str(config['pseudopowerdir']+'/Alm2Cl -I ' + Overdensity_Flask_Alm[j]
                                 + ' -O ' + config['over_flaskmaps_autocl'] + '/z'+str(j)+'z'+str(i) + '/FlaskAutoCl-'
                                 + str(k) + '.dat'
                                 + ' -m ' +  config['footprintmask'] 
                                 + ' -o -R ' + config['footprint_ilmjlm']
                                 + ' -N ' + str(config['nside']) 
                                 + ' -L ' + str(config['lmax'])
                                 + ' -c ' + Overdensity_Flask_Alm[i] + ' ' + Overdensity_FLASK_maps[i])
                    os.system(comand)
                    
    # Density_to_Overdensity Flask maps: Alm2Cl - FLASKmapbin[j] - Cross - FLASKmapbin[i]
    if int(config['dens_to_over_flask_autocl_calc'])==1:

        #Checking if paths exist
        for j in range(len(Dens_to_Over_Flask_Alm)):
            for i in range(j,len(Dens_to_Over_Flask_Alm)):
                # Does the path exist? if not:
                if glob.glob(str(config['dens_to_over_flaskmaps_autocl'] + '/z'+str(j)+'z'+str(i))) == []: 
                    # Folder creation for Overdensity Cl's.
                    for j in range(len(Dens_to_Over_Flask_Alm)):
                        for i in range(j,len(Dens_to_Over_Flask_Alm)):
                            # mkdir -p, --parents. No error if existing, make parent directories as needed.
                            os.system(str('mkdir -p ' + config['dens_to_over_flaskmaps_autocl'] + '/z'+str(j)+'z'+str(i) )) 
                    break
                
        # Cl's calc:
        for j in range(len(Dens_to_Over_Flask_Alm)):
            for i in range(j,len(Dens_to_Over_Flask_Alm)):
                # Auto-Cl:
                if i == j:
                    comand = str(config['pseudopowerdir']+'/Alm2Cl -I ' + Dens_to_Over_Flask_Alm[j]
                                 + ' -O ' + config['dens_to_over_flaskmaps_autocl'] + '/z'+str(j)+'z'+str(i) + '/FlaskAutoCl-'
                                 + str(k) + '.dat'
                                 + ' -m ' +  config['footprintmask'] 
                                 + ' -o -R ' + config['footprint_ilmjlm'] 
                                 + ' -N ' + str(config['nside']) 
                                 + ' -L ' + str(config['lmax']) )
                    os.system(comand)
                # Cross-Cl:
                else:
                    comand = str(config['pseudopowerdir']+'/Alm2Cl -I ' + Dens_to_Over_Flask_Alm[j]
                                 + ' -O ' + config['dens_to_over_flaskmaps_autocl'] + '/z'+str(j)+'z'+str(i) + '/FlaskAutoCl-'
                                 + str(k) + '.dat'
                                 + ' -m ' +  config['footprintmask'] 
                                 + ' -o -R ' + config['footprint_ilmjlm'] 
                                 + ' -N ' + str(config['nside']) 
                                 + ' -L ' + str(config['lmax'])
                                 + ' -c ' + Dens_to_Over_Flask_Alm[i] + ' ' + Dens_to_Over_FLASK_maps[i])
                    os.system(comand)
    
    # === Systemátics Maps Cl's ===
    
    # Overdensity FlaskMaps-Systematics: Alm2Cl
    if int(config['over_flaskmaps_cross_sys_calc'])==1:
        folderSys = natsorted(glob.glob(str(config['systematicsmaps'] + '/*.fits')))
        folderSysAlms = natsorted(glob.glob(str(config['sys_almdir'] + '/*.fits')))

        for l in range(len(Overdensity_FLASK_maps)):
            for m in range(len(folderSys)):
                path = str(config['over_flaskmaps_cross_sys'] + '/bin'+str(l) + '/'
                            + folderSys[m][len(config['systematicsmaps'] +'/'
                            + config['sys_prefix']):-len(config['sys_sufix'])])
                if glob.glob(path)==[]:
                    os.system(str('mkdir -p ' + path))

                comand = str(config['pseudopowerdir']+'/Alm2Cl -I ' + Overdensity_Flask_Alm[l] 
                            + ' -O ' + config['over_flaskmaps_cross_sys']+'/bin'+str(l)+'/'
                            + folderSys[m][len(config['systematicsmaps'] +'/' + config['sys_prefix'] ):-len(config['sys_sufix'])] +'/bin'+str(l)
                            + '-c-' + folderSys[m][len(config['systematicsmaps'] +'/'
                            + config['sys_prefix'] ):-len(config['sys_sufix'])]+'-'+f'{k:04d}'+'.dat' 
                            + ' -m ' +  config['footprintmask'] 
                            + ' -o -R ' + config['footprint_ilmjlm']
                            + ' -N ' + str(config['nside']) + ' -L ' + str(config['lmax'])
                            + ' -c ' + folderSysAlms[m] + ' ' + folderSys[m])
                os.system(comand)

                
                
    # Density to Overdensity FlaskMaps-Systematics: Alm2Cl
    if int(config['dens_to_over_flaskmaps_cross_sys_calc'])==1:
        folderSys = natsorted(glob.glob(str(config['systematicsmaps'] + '/*.fits')))
        folderSysAlms = natsorted(glob.glob(str(config['sys_almdir'] + '/*.fits')))

        for l in range(len(Dens_to_Over_FLASK_maps)):
            for m in range(len(folderSys)):
                path = str(config['dens_to_over_flaskmaps_cross_sys'] + '/bin'+str(l) + '/'
                        + folderSys[m][len(config['systematicsmaps'] +'/'
                        + config['sys_prefix']):-len( config['sys_sufix'] )])
                if glob.glob(path)==[]:
                    os.system(str('mkdir -p ' + path))

                comand = str(config['pseudopowerdir']+'/Alm2Cl -I ' + Dens_to_Over_Flask_Alm[l] 
                            + ' -O ' + config['dens_to_over_flaskmaps_cross_sys']+'/bin'+str(l)+'/'
                            + folderSys[m][len(config['systematicsmaps'] +'/' + config['sys_prefix'] ):-len(config['sys_sufix'])] +'/bin'+str(l)
                            + '-c-' + folderSys[m][len(config['systematicsmaps'] +'/' + config['sys_prefix'] ):-len(config['sys_sufix'])]+'-'+f'{k:04d}'+'.dat' 
                            + ' -m ' +  config['footprintmask'] 
                            + ' -o -R ' + config['footprint_ilmjlm']
                            + ' -N ' + str(config['nside']) + ' -L ' + str(config['lmax'])
                            + ' -c ' + folderSysAlms[m] + ' ' + folderSys[m])
                os.system(comand)
                
                
                
    print('Simulation: '+str(k+1)+'/'+str(int(config['nflaskmaps'])-int(config['start_in'])))
    
    # Remove flask alm.fits
    os.system('rm '+ config['over_flaskmaps_almdir'] + config['mapfits_prefix'][len(config['flaskmapsdir']):] + str(k) + "*" )
    os.system('rm '+ config['dens_to_over_flaskmaps_almdir'] + config['mapwerfits_prefix'][len(config['flaskmapsdir']):] + str(k) + "*" )
    
    # Remove flask maps.fits
    os.system('rm '+ config['mapfits_prefix'] + str(k)+"*" )
    os.system('rm '+ config['mapwerfits_prefix'] + str(k)+"*" )
