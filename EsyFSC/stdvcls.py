# Compute the standard deviation of Cl's.
# 
# Author: Felipe SC
# Email: felipesc.physis@gmail.com, felipesc.physis@protonmail.com
# Project: BINGO

from statistics import stdev

import os 
import glob
import pandas as pd

def stdvcls(path, systematics=False):
    """
    Compute the standard deviation of Cl's.
    path = list of folders containing cl's.

    Return: Write a file std-erro.dat containing the standard deviation in the respective folder.
    """
    
    # skip rows of .dat file.
    if systematics: 
        skip = 3
        
    for j in range(len(path)):
        # skip rows of .dat file if not systematics maps:
        if not systematics:
            z = path[j].split("/")[-1].split('z')
            if z[1]==z[2]:
                skip = 1
            else:
                skip = 3
        
        datFiles = glob.glob(str(path[j]+'/*'))
        matrixCls = pd.DataFrame() # Temporary variable. 
        for k in range(len(datFiles)):
            opencls = pd.read_table(datFiles[k],
                                    header=0,
                                    names=('l','cl'), 
                                    delim_whitespace=True,
                                    skiprows=skip)
            matrixCls[str('cl'+str(k))] = opencls['cl']

        stdevlist = []
        for k in range(len(matrixCls['cl0'])):
            tolist = matrixCls.transpose()[k].values.tolist()
            stdevlist.append(stdev(tolist))

        write = pd.DataFrame(stdevlist)
        savePath = str(path[j] + '/std-erro.dat') 
        tfile = open(savePath, 'a')
        tfile.write(write.to_string(index=True,
                                       na_rep='0',
                                       index_names=False,
                                       header=False) )
        tfile.close()
