# GENERATE "SELECTION RADIAL FUNCTION" FILE FOR FLASK USE.
#
# Author: Felipe SC
# Email: felipesc.physis@gmail.com, felipesc.physis@protonmail.com
# Project: BINGO

import numpy as np


# Path to save SelectionRadialFunction.dat
PATH = 'PATH/flask/data/example-z-selection-f1.dat'

# Number of unmasked pixels.
npix_unmasked = 780081 

# Densities of sky maps, contained in maps.fits
ntot_weighted = np.asarray([127965.8345203514,
                            253058.4309778168,
                            460207.1356194952,
                            575762.3607539533,
                            629783.8138515665,
                            639377.8797019095,
                            593565.1129589592,
                            541703.6634712955,
                            456173.5018747653,
                            364873.6792312067,
                            289499.0929090777,
                            245367.4018230989,
                            207064.8406644868,
                            49410.74272316132])

# Redshift intervals
intervalo_z = [0.000,0.050,0.075,0.100,0.125,0.150,0.175,0.200,0.225,0.250,0.275,0.300,0.325,0.350,0.400]

# rsf = [1 for k in range(14)]  # rsf=constante for 14bins

# Radial Selection Function: Adjusted values
rsf = np.array([ 0.0635,
                0.268,
                0.498,
                0.625,
                0.682,
                0.695,
                0.64,
                0.584,
                0.495,
                0.3951,
                0.318,
                0.273,
                0.243,
                0.0292])

os.remove(PATH)

delta = 10
dz = []
for k in range(len(intervalo_z)-1):
    dz.append(np.float16(intervalo_z[k+1]-intervalo_z[k]))

for k in range(14):
    delta_z = dz[k]/(delta-1)
    z = intervalo_z[k]
    
    for j in range(delta):
        if j+1 == delta and k<len(rsf)-1:
            open(PATH,'a').write(f'{z:.3f}\t{((rsf[k]+rsf[k+1])/2):.7f}\n')
        else:
            open(PATH,'a').write(f'{z:.3f}\t{rsf[k]:.7f}\n')
        z = z + delta_z
