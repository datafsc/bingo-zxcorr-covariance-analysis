# Converts the .fits map in NESTED format to RING format.
#
# Author: Felipe SC
# Email: felipesc.physis@gmail.com, felipesc.physis@protonmail.com
# Project: BINGO

import healpy as hp

def convert(path = ""):
    """
    Converts the .fits map in NESTED format to RING format.

    Input: PATH/map.fits in NESTED format.
    Return: Saves the converted map in the same folder named InputName_RING.fits.
    """
    # Converting
    mapa = hp.fitsfunc.read_map(path,nest=None) # Loading PATH/Map_NESTED.fits
    hp.pixelfunc.reorder(mapa, n2r=True)
    
    # Saving
    pathSave = path[:-5] + "_RING.fits"
    hp.fitsfunc.write_map(pathSave, hp.pixelfunc.reorder(mapa, n2r=True))

