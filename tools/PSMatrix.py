# Creates the power spectrum matrix in UCLCL format.
#
# Author: Felipe SC
# Email: felipesc.physis@gmail.com, felipesc.physis@protonmail.com
# Project: BINGO

import numpy as np
import glob
from natsort import natsorted


def PSMatrix(path, savePath="./SlMatrix.dat"):
    """
    Creates the power spectrum matrix.

    input:
    path = PATH/cls; Path of the folder that contains the cls files.
    savePath = PATH/PSMatrix.dat
    
    output: PSMatrix.dat; matrix of Power Spectrum in UCLCL format.
    """
    
    dataPath = natsorted(glob.glob(path+"/*z*z*.dat"))
    
    for k in range(len(dataPath)):
        ps = np.loadtxt(dataPath[k])
        if k==0: # fist interation, we will take l and PowerSpectrum.
            concat = ps
            continue
            
        ps = ps[:,1].reshape((len(ps[:,1]),1))
        #concatenating
        concat = np.concatenate([concat,ps],axis=1)
    
    pathSave = "./SlMatrix.dat"
    np.savetxt(pathSave, matriz, fmt='%.5e')


if __name__ == "__main__":

path = './binedSl'
matriz = PSMatrix(path)


